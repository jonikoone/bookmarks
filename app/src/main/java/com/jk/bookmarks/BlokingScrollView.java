package com.jk.bookmarks;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.ScrollView;

public class BlokingScrollView extends ScrollView {

    private boolean scrollingEnabled = true;

    public BlokingScrollView(Context context) {
        super(context);
    }

    public BlokingScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BlokingScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public BlokingScrollView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
//        if (scrollingEnabled() && motion(ev)) {
//            return super.onInterceptTouchEvent(ev);
//        } else {
//            return false;
//        }
        return super.onInterceptTouchEvent(ev);
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
//        if (scrollingEnabled() && motion(ev))
//            return super.onTouchEvent(ev);
//        else
//            return false;

        switch (ev.getAction()){
            case MotionEvent.ACTION_DOWN:
                this.getParent().requestDisallowInterceptTouchEvent(true);
                break;
            case MotionEvent.ACTION_UP:
                this.getParent().requestDisallowInterceptTouchEvent(false);
                break;
        }

        super.onTouchEvent(ev);
        return true;
    }
    float downY;
    float delta;
    private boolean motion(MotionEvent ev){
        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                //downY = ev.getRawY();
                this.getParent().requestDisallowInterceptTouchEvent(true);
                break;

            case MotionEvent.ACTION_MOVE:
//                delta = ev.getRawY() - downY;
//                //двигаем вниз
//                if (ev.getRawY() > downY && delta < 100 && delta >= 0)
//                    return false;
//                //двигаем вверх
//                else if (ev.getRawY() < downY && delta < -100 && delta <= 0)
//                    return false;
//                else
//                    return true;
                break;

            case MotionEvent.ACTION_UP:
//                this.getParent().requestDisallowInterceptTouchEvent(false);
                break;
//                return true;
        }
        return false;
    }

    private boolean scrollingEnabled(){
        return scrollingEnabled;
    }

    public void setScrolling(boolean scrollingEnabled) {
        this.scrollingEnabled = scrollingEnabled;
    }

}
