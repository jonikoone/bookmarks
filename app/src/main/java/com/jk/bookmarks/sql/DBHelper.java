package com.jk.bookmarks.sql;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;

import com.jk.bookmarks.MainActivity;
import com.jk.bookmarks.data.BookmarkModel;

public class DBHelper extends SQLiteOpenHelper{

    public final static String TABLE = "bookmarks";
    public final static String ID = "id";
    public final static String NAME = "name";
    public final static String CONTENT = "content";
    public final static String DATETIME = "datetime";

    public DBHelper(Context context) {
        super(context, TABLE, null, 1);
    }

    public DBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + TABLE + " ("
                + ID + " integer primary key autoincrement,"
                + DATETIME + " integer,"
                + NAME + " text,"
                + CONTENT + " text);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public SQLiteDatabase getSQLiteDatabase(){
        return this.getSQLiteDatabase();
    }

    public ContentValues getContentValues(@NonNull BookmarkModel model){
        ContentValues contentValues = new ContentValues();
        contentValues.put(NAME, model.getName());
        contentValues.put(CONTENT, model.getContent());
        contentValues.put(DATETIME, model.getDateTime());
        return contentValues;
    }

    public long saveBookmark(SQLiteDatabase db, ContentValues contentValues){
        return db.insert(TABLE, null, contentValues);
    }
}
