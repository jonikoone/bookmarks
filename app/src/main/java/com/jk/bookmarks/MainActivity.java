package com.jk.bookmarks;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jk.bookmarks.data.BookmarkModel;
import com.jk.bookmarks.sql.DBHelper;

import java.util.List;

import static com.jk.bookmarks.OpenBookmark.BOOKMARK_DELETE;
import static com.jk.bookmarks.OpenBookmark.BOOKMARK_SAVE;

public class MainActivity extends AppCompatActivity{

    private LinearLayout mainLayout;

    private List<BookmarkModel> allBookmarks;

    private Menu mainMenu;
    private BlokingScrollView mainScroll;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mainLayout = findViewById(R.id.mainBookmarkList);
        mainScroll = findViewById(R.id.mainScroll);
        updateList();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_main_activity, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()){
            case R.id.mainMenu_createNewBookmark:
                //создаем новую заметку
                intent = new Intent("com.jk.action.intents.OPEN_BOOKMARK_CREATE");
                startActivityForResult(intent, 1);
                break;
            case R.id.mainMenu_cleanBase:
                //полностью очищаем базу
                final ConfirmDeletion cd = new ConfirmDeletion(this);
                cd.setClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        switch (v.getId()){
                            case R.id.delete_confirm_deletion:
                                DBHelper dbHelper = new DBHelper(getApplicationContext());
                                SQLiteDatabase writableDatabase = dbHelper.getWritableDatabase();
                                writableDatabase.delete(DBHelper.TABLE, null, null);
                                dbHelper.close();
                                updateList();
                                break;
                        }
                        cd.dismiss();
                    }
                });
                cd.show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void updateList(){
        allBookmarks = BookmarkModel.getAllBookmarks(getApplicationContext());
        mainLayout.removeAllViews();
        if (allBookmarks.size() != 0) {
            for (BookmarkModel model : allBookmarks) {
                mainLayout.addView(new BookmarkView(this, mainScroll, model, this));
            }
        } else {
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
            params.gravity = Gravity.CENTER;
            TextView textView = new TextView(this);
            textView.setText(getResources().getString(R.string.emptyBaseAlert));
            textView.setTextSize(getResources().getDimensionPixelSize(R.dimen.text_size));
            textView.setTextColor(getResources().getColor(R.color.colorTextDark));
            mainLayout.addView(textView, params);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("delete", String.valueOf(resultCode));
        switch (resultCode){
            case BOOKMARK_SAVE:
                updateList();
                break;
            case BOOKMARK_DELETE:
                updateList();
                break;
        }
    }

}
