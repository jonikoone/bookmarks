package com.jk.bookmarks;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.jk.bookmarks.data.BookmarkModel;
import com.jk.bookmarks.sql.DBHelper;

public class OpenBookmark extends AppCompatActivity implements View.OnClickListener {

    public static final int BOOKMARK_SAVE = -2931;
    public static final int BOOKMARK_DELETE = -2932;

    private Button btnSave, btnDel;
    private EditText txtName, txtContent;
    private TextView datetime;
    private BookmarkModel model;

    //для определения дальшейших действий над закладкой
    private boolean isCreateBookmark = true;
    //если форма открыта из другого приложения
    private boolean isShare = false;

    private DBHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_open_bookmark);

        btnSave = findViewById(R.id.buttonSaveBookmark);
        btnDel = findViewById(R.id.buttonDeleteBookmark);
        txtName = findViewById(R.id.nameBookmark);
        txtContent = findViewById(R.id.contentBookmark);
        datetime = findViewById(R.id.datetime);
        dbHelper = new DBHelper(getApplicationContext());

        btnSave.setOnClickListener(this);
        btnDel.setOnClickListener(this);

        Intent intent = getIntent();
        switch (intent.getAction()){
            case "com.jk.action.intents.OPEN_BOOKMARK_CHANGE":
                isCreateBookmark = false;
                int id = intent.getIntExtra(DBHelper.ID, 0);
                Cursor cursor = dbHelper.getWritableDatabase().query(DBHelper.TABLE, null, DBHelper.ID + " = ?", new String[]{String.valueOf(id)}, null, null, null );
                if (cursor.moveToFirst()){
                    model = new BookmarkModel(cursor);
                    txtName.setText(model.getName());
                    txtContent.setText(model.getContent());
                    datetime.setText(model.getFormatedDateTime());
                }

                break;
            case "com.jk.action.intents.OPEN_BOOKMARK_CREATE":
                btnDel.setVisibility(View.GONE);
                break;
            case "android.intent.action.SEND":
                txtContent.setText(intent.getStringExtra("android.intent.extra.TEXT"));
                btnDel.setVisibility(View.GONE);
                isShare = true;
                break;
        }


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.buttonDeleteBookmark:{
                SQLiteDatabase sqLiteDatabase = dbHelper.getWritableDatabase();
                sqLiteDatabase.delete(DBHelper.TABLE, DBHelper.ID + " = ?", new String[]{String.valueOf(model.getId())});
                dbHelper.close();
                setResult(BOOKMARK_DELETE);
                finish();
                break;
            }
            case R.id.buttonSaveBookmark:{
                SQLiteDatabase sqLiteDatabase = dbHelper.getWritableDatabase();
                if (isCreateBookmark)
                    model = new BookmarkModel();
                model.update(txtName.getText().toString(), txtContent.getText().toString());
                if (isCreateBookmark)
                    sqLiteDatabase.insert(DBHelper.TABLE, null, dbHelper.getContentValues(model));
                else
                    sqLiteDatabase.update(DBHelper.TABLE, dbHelper.getContentValues(model), DBHelper.ID + " = ?", new String[]{String.valueOf(model.getId())});
                setResult(OpenBookmark.BOOKMARK_SAVE);
                dbHelper.close();
                if (isShare) { // если активити запускается через share то после сохранения открываем main
                    Intent intent = new Intent(this, MainActivity.class);
                    startActivity(intent);
                }
                    finish();
                break;
            }
        }
    }
}
