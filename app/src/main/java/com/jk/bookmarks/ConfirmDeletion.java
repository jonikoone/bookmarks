package com.jk.bookmarks;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.jk.bookmarks.sql.DBHelper;

public class ConfirmDeletion extends AlertDialog{

    private View.OnClickListener clickListener;

    Button remove, cancel;

    protected ConfirmDeletion(@NonNull Context context) {
        super(context);
    }

    public void setClickListener(View.OnClickListener clickListener){
        this.clickListener = clickListener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_deletion);
        remove = findViewById(R.id.delete_confirm_deletion);
        cancel = findViewById(R.id.cancel_confirm_deletion);

        remove.setOnClickListener(clickListener);
        cancel.setOnClickListener(clickListener);

    }
}
