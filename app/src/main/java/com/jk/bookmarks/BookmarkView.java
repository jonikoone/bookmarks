package com.jk.bookmarks;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.jk.bookmarks.data.BookmarkModel;
import com.jk.bookmarks.sql.DBHelper;

/**
 * Элемент закладки в списке
 */
public class BookmarkView extends FrameLayout implements View.OnClickListener, View.OnTouchListener {

    TextView name, datetime, content;
    BookmarkModel bookmarkModel;
    FrameLayout slide;
    FrameLayout deleteFrame, changeFrame;
    Activity ownerActivity;
    BlokingScrollView mainScroll;
    int maxDeviation;
    int downX = 0;
    int left = 0;
    int rigth = 0;
    int currentLeft = 0;
    int currentRigth = 0;

    public BookmarkView(Context context, BlokingScrollView mainScroll, Activity ownerActivity) {
        this(context, mainScroll, new BookmarkModel(), ownerActivity);
    }

    public BookmarkView(Context context, BlokingScrollView mainScroll, BookmarkModel bookmarkModel, Activity ownerActivity) {
        super(context);
        this.ownerActivity = ownerActivity;
        this.bookmarkModel = bookmarkModel;
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.bookmark_view, this);
        this.setOnClickListener(this);
        name = findViewById(R.id.bmv_name);
        datetime = findViewById(R.id.bmv_datetime);
        content = findViewById(R.id.bmv_content);
        slide = findViewById(R.id.slidePanel);
        deleteFrame = findViewById(R.id.slideActionDelete);
        changeFrame = findViewById(R.id.slideActionChange);

        slide.setOnTouchListener(this);
        name.setText(bookmarkModel.getName());
        datetime.setText(bookmarkModel.getPostFormatedDate());
        content.setText(bookmarkModel.getContent());
        deleteFrame.setOnClickListener(this);
        changeFrame.setOnClickListener(this);

        this.mainScroll = mainScroll;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.slideActionDelete:
                Log.d("slide", "del");
                final ConfirmDeletion cd = new ConfirmDeletion(ownerActivity);
                cd.setClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        switch (v.getId()){
                            case R.id.delete_confirm_deletion:
                                DBHelper dbHelper = new DBHelper(ownerActivity.getApplicationContext());
                                SQLiteDatabase writableDatabase = dbHelper.getWritableDatabase();
                                writableDatabase.delete(DBHelper.TABLE, DBHelper.ID + " = ?", new String[]{String.valueOf(bookmarkModel.getId())});
                                dbHelper.close();
                                ((MainActivity)ownerActivity).updateList();
                                break;
                        }
                        cd.dismiss();
                    }
                });
                cd.show();
                break;
            case R.id.slideActionChange:
                Log.d("slide", "change");
                Intent openBookmarkIntent = new Intent("com.jk.action.intents.OPEN_BOOKMARK_CHANGE");
//                openBookmarkIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                openBookmarkIntent.putExtra(DBHelper.ID, bookmarkModel.getId());
                ownerActivity.startActivityForResult(openBookmarkIntent, bookmarkModel.getId());
                break;
        }
    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        maxDeviation = this.getWidth()/2;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        int x = (int) event.getRawX();

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                downX = x;
                mainScroll.setScrolling(false);
                break;
            case MotionEvent.ACTION_MOVE:
                left = x - downX;
                rigth = downX - x;
                if (left > 100 || left < -100) {
                    this.getParent().requestDisallowInterceptTouchEvent(true);
                    MarginLayoutParams layoutParams = (MarginLayoutParams) slide.getLayoutParams();
                    if (currentLeft != 0 & currentRigth != 0) {
                        left += currentLeft;
                        rigth += currentRigth;
                    }

                    if (left < maxDeviation & left > -maxDeviation & rigth < maxDeviation & rigth > -maxDeviation) {
                        layoutParams.rightMargin = rigth;
                        layoutParams.leftMargin = left;
                    } else {
                        if (x > downX) {
                            left = maxDeviation;
                            rigth = -maxDeviation;
                        } else {
                            left = -maxDeviation;
                            rigth = maxDeviation;
                        }
                    }

                    slide.setLayoutParams(layoutParams);
                }else
                    return false;
                break;

            case MotionEvent.ACTION_UP:
                mainScroll.setSmoothScrollingEnabled(true);
                this.getParent().requestDisallowInterceptTouchEvent(false);
                final MarginLayoutParams layoutParams1 = (MarginLayoutParams) slide.getLayoutParams();
                if (left < maxDeviation/2 & left > -maxDeviation/2 | rigth < -maxDeviation/2 & rigth > maxDeviation/2) {
                    layoutParams1.rightMargin = 0;
                    layoutParams1.leftMargin = 0;
                    currentRigth = 0;
                    currentLeft = 0;
                    slide.setLayoutParams(layoutParams1);

                } else {
                    if (left > 0)
                        layoutParams1.leftMargin = maxDeviation;
                    else if (rigth > 0)
                        layoutParams1.rightMargin = maxDeviation;
                    currentRigth = rigth;
                    currentLeft = left;
                }
                slide.setLayoutParams(layoutParams1);
                mainScroll.setScrolling(true);
                break;
        }

        return true;
    }
}
