package com.jk.bookmarks.data;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.jk.bookmarks.sql.DBHelper;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.TimeZone;

/*
 * Модель заметки и методы для работы с бд
 * */
public class BookmarkModel {

    private int id;
    private String name, content;
    private Date dateTime;

    public BookmarkModel() {
        id = 0;
        name = "Пустая модель";
        content = "Пустая модель";
        dateTime = new Date();
    }

    public BookmarkModel(int id, String name, String content, long dateTime) {
        this(id, name, content, new Date(dateTime));
    }

    public BookmarkModel(int id, String name, String content, Date dateTime) {
        this.id = id;
        this.name = name;
        this.content = content;
        this.dateTime = dateTime;
    }

    public BookmarkModel(Cursor cursor) {
        this.id = cursor.getInt(cursor.getColumnIndex(DBHelper.ID));
        this.name = cursor.getString(cursor.getColumnIndex(DBHelper.NAME));
        this.content = cursor.getString(cursor.getColumnIndex(DBHelper.CONTENT));
        this.dateTime = new Date(cursor.getLong(cursor.getColumnIndex(DBHelper.DATETIME)));
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public long getDateTime() {
        return dateTime.getTime();
    }

    public String getFormatedDateTime() {
        return new SimpleDateFormat("dd MMMM yy - hh:mm").format(dateTime);
    }

    public String getPostFormatedDate() {
        //показываем как давно была сделана заметка
        Date currentDate = new Date();
        long d3 = currentDate.getTime() - dateTime.getTime();
        long countElemDate;
        if ((countElemDate = (d3 / (1000 * 60 * 60 * 24))) > 0)
            return new SimpleDateFormat("dd:MM:yy").format(dateTime);
        else if ((countElemDate = (d3 / (1000 * 60 * 60))) > 0)
            return countElemDate + "час.н.";
        else if ((countElemDate = (d3 / (1000 * 60))) > 0)
            return countElemDate + "мин.н.";
        else if ((countElemDate = (d3 / (1000))) > 0)
            return countElemDate + "сек.н.";
        return "сейчас";
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public static List<BookmarkModel> getAllBookmarks(Context context) {
        DBHelper dbHelper = new DBHelper(context);
        SQLiteDatabase sqLiteDatabase = dbHelper.getWritableDatabase();
        Cursor cursor = sqLiteDatabase.query(DBHelper.TABLE, null, null, null, null, null, null);
        List<BookmarkModel> bookmarkModels = new LinkedList<>();
        while (cursor.moveToNext()) {
            BookmarkModel model = new BookmarkModel(
                    cursor.getInt(cursor.getColumnIndex(DBHelper.ID)),
                    cursor.getString(cursor.getColumnIndex(DBHelper.NAME)),
                    cursor.getString(cursor.getColumnIndex(DBHelper.CONTENT)),
                    cursor.getLong(cursor.getColumnIndex(DBHelper.DATETIME))
            );
            bookmarkModels.add(model);
        }
        return bookmarkModels;
    }

    public static BookmarkModel getBookmarkById() {
        return null;
    }

    public void update(String name, String content) {
        this.name = name;
        this.content = content;
    }

}
